package br.prefeitura.agenda.consulta.model

import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.Future
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Table(name = "evento_agenda")
data class Agendamento(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @get: NotNull
    @get: Future
    val data: LocalDateTime = LocalDateTime.now(),

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "paciente", referencedColumnName = "id")
    @get: NotNull
    val paciente: Paciente,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "unidade", referencedColumnName = "id")
    @get: NotNull
    val unidade: Unidade,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "profissional", referencedColumnName = "id")
    @get: NotNull
    val profissional: Profissional
)

@Entity
data class Profissional(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @get: NotBlank
    val nome: String = "",

    @get: NotBlank
    val matricula: String = "",

    @get: NotBlank
    val registroConselho: String = ""
)

@Entity
data class Paciente(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @get: NotBlank
    val nome: String = "",

    @get: NotBlank
    val documento: String = ""
)

@Entity
data class Unidade(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @get: NotBlank
    val nome: String = ""
)
