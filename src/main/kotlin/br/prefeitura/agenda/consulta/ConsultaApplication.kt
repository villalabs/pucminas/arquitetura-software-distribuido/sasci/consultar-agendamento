package br.prefeitura.agenda.consulta

import com.google.common.base.Predicates
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@SpringBootApplication class ConsultaApplication

fun main(args: Array<String>) {
    runApplication<ConsultaApplication>(*args)
}

@Configuration
@EnableSwagger2
class SwaggerConfig : WebMvcConfigurer {

    @Bean
    fun api() =
            Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(
                            Predicates.not(
                                    RequestHandlerSelectors.basePackage(
                                            "org.springframework.boot")))
                    .build()

    @Override
    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/")
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/")
    }

    private fun apiInfo(): ApiInfo {
        return ApiInfo("Consulta de Agendamentos", "", "", "", Contact("", "", ""), "", "")
    }
}
