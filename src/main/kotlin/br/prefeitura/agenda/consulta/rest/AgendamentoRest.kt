package br.prefeitura.agenda.consulta.rest

import br.prefeitura.agenda.consulta.service.AgendamentoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/agendamentos")
class AgendamentoRest {

    @Autowired
    lateinit var agendamentoService: AgendamentoService

    @GetMapping
    fun getAgendamentos(pageable: Pageable) = agendamentoService.getAgendamentos(pageable)

    @GetMapping("/paciente/{idPaciente}")
    fun getAgendamentosByPaciente(@PathVariable idPaciente: Long, pageable: Pageable) =
        agendamentoService.getAgendamentosByPaciente(idPaciente, pageable)

    @GetMapping("/unidade/{idUnidade}")
    fun getAgendamentosByUnidade(@PathVariable idUnidade: Long, pageable: Pageable) =
        agendamentoService.getAgendamentosByUnidade(idUnidade, pageable)

    @GetMapping("/profissional/{idProfissional}")
    fun getAgendamentosByProfissional(@PathVariable idProfissional: Long, pageable: Pageable) =
        agendamentoService.getAgendamentosByProfissional(idProfissional, pageable)
}