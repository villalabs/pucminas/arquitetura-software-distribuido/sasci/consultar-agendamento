package br.prefeitura.agenda.consulta.service

import br.prefeitura.agenda.consulta.model.Agendamento
import br.prefeitura.agenda.consulta.model.Paciente
import br.prefeitura.agenda.consulta.repository.AgendamentoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.UUID
import kotlin.random.Random

@Service
class AgendamentoService {

    @Autowired
    lateinit var agendamentoRepository: AgendamentoRepository

    fun getAgendamentos(pageable: Pageable) = agendamentoRepository.findAll(pageable)

    fun getAgendamentosByPaciente(idPaciente: Long, pageable: Pageable) =
        agendamentoRepository.findAllByPacienteId(idPaciente, pageable)

    fun getAgendamentosByUnidade(idUnidade: Long, pageable: Pageable) =
        agendamentoRepository.findAllByUnidadeId(idUnidade, pageable)

    fun getAgendamentosByProfissional(idProfissional: Long, pageable: Pageable) =
        agendamentoRepository.findAllByProfissionalId(idProfissional, pageable)
}