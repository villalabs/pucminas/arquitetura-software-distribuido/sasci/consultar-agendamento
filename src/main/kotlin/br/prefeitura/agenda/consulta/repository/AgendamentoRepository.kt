package br.prefeitura.agenda.consulta.repository

import br.prefeitura.agenda.consulta.model.Agendamento
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AgendamentoRepository : JpaRepository<Agendamento, Long> {

    fun findAllByPacienteId(pacienteId: Long, pageable: Pageable): Page<Agendamento>

    fun findAllByUnidadeId(unidadeId: Long, pageable: Pageable): Page<Agendamento>

    fun findAllByProfissionalId(profissionalId: Long, pageable: Pageable): Page<Agendamento>
}
